﻿using Assets.Scripts.Game.Components;
using UnityEngine;

namespace Assets.Scripts.Level
{
    public class UnitInfo
    {
        public int TeamId;

        public AbilityType AbilityType;

        public EffectType EffectType;

        public Vector3 Position;

        public int HP;

        public int Attack;
    }
}
