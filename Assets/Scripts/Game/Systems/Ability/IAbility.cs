﻿using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Systems.Ability
{
    public interface IAbility {
        void Apply(UnitParams sourceParams, UnitParams targetParams);
    }
}
