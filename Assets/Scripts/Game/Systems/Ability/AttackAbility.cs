﻿using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Systems.Ability
{
    public class AttackAbility: IAbility
    {
        public void Apply(UnitParams sourceParams, UnitParams targetParams) {

            targetParams.HP -= sourceParams.Attack;

        }
    }
}
