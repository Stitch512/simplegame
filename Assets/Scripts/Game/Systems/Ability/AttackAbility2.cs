﻿
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Systems.Ability
{
    public class AttackAbility2: IAbility {
        private readonly IEntityEngine _engine;

        public AttackAbility2(IEntityEngine engine) {
            _engine = engine;
        }

        public void Apply(UnitParams sourceParams, UnitParams targetParams) {
            targetParams.HP -= sourceParams.Attack / 2;

            var reduce = _engine.AddComponent<AttackReduce>(targetParams.Owner);
            reduce.Reduce = 3;
            reduce.StepsCount = 2;
        }
    }
}
