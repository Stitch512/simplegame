﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Systems
{
    public class UnitAttackProcessing: BaseEntitySystem
    {
        public UnitAttackProcessing(IEntityEngine entityEngine) : base(entityEngine) {

        }

        public override void Execute(CustomObject data) {
            var session = Engine.GetComponents<GameSession>().FirstOrDefault();
            if (session == null)
                return;

            var units = Engine.GetComponents<UnitTeam>(c => c.Team == session.CurrentTeam);
        }
    }
}
