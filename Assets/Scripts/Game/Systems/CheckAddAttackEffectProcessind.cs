﻿using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Systems {
    public class CheckAddAttackEffectProcessind : BaseEntitySystem {
        private readonly IEventsManager<GameEventType> _eventsManager;

        public CheckAddAttackEffectProcessind(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) :
            base(entityEngine) {
            _eventsManager = eventsManager;
        }

        public override void Execute(CustomObject data) {
            var session = Engine.GetComponent<GameSession>();
            if (session == null)
                return;

            var effectUnits = Engine.GetComponents<AddAttackEffect>();
            foreach (var effectUnit in effectUnits) {
                if (!session.DamagedUnits.Contains(effectUnit.Owner.Id))
                    continue;
                var unitParams = effectUnit.Owner.GetComponent<UnitParams>();
                if (unitParams == null)
                    continue;

                unitParams.Attack += effectUnit.AddedAttack;

                _eventsManager.Invoke(GameEventType.UpdateUnitParams,
                    new CustomObject(new UpdateUnitParamsEventData {
                        UnitsParams = new List<UpdateUnitParams> {
                            new UpdateUnitParams {
                                UnitId = new ElementId(ElementType.Unit, unitParams.Owner.Id),
                                Attack = new ItemValue<int> {Value = unitParams.Attack}
                            }
                        }
                    }));
            }

            session.DamagedUnits.Clear();
        }
    }
}