﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Systems
{
    public class SelectUnitProcessing: BaseEntitySystem
    {
        public SelectUnitProcessing(IEntityEngine entityEngine) : base(entityEngine) { }
        public override void Execute(CustomObject data) {
            var elementId = data.Value as ElementId;
            if (elementId == null)
                return;

            var session = Engine.GetComponents<GameSession>().FirstOrDefault();
            if (session == null || session.SessionState != GameSessionState.WaitAction)
                return;

            if (session.CurrentTeam != session.MyTeam)
                return;

            var entity = Engine.GetEntity(elementId.Id);
            var entityTeam = entity.GetComponent<UnitTeam>().Team;
            if (entityTeam == session.ActiveUnit?.GetComponent<UnitTeam>().Team)
                return;
            session.SelectedUnit = entity;
        }
    }
}
