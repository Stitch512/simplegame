﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Systems
{
    public class CheckBleedingProcessing : BaseEntitySystem
    {
        private readonly IEventsManager<GameEventType> _eventsManager;

        public CheckBleedingProcessing(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) : base(
            entityEngine)
        {
            _eventsManager = eventsManager;
        }

        public override void Execute(CustomObject data)
        {
            var updateEventData = new UpdateUnitParamsEventData();

            /*var deathUnits = Engine.GetComponents<UnitParams>(c => c.HP <= 0).ToList();
            foreach (var deathUnit in deathUnits) {
                
            }*/

            var bleedings = Engine.GetComponents<UnitBleeding>().ToList();
            foreach (var bleeding in bleedings)
            {
                var unitParams = bleeding.Owner.GetComponent<UnitParams>();
                if (unitParams == null)
                    continue;

                unitParams.HP -= bleeding.IncrementValue;
                updateEventData.UnitsParams.Add(new UpdateUnitParams
                {
                    UnitId = new ElementId(ElementType.Unit, unitParams.Owner.Id),
                    HP = new ItemValue<int> { Value = unitParams.HP }
                });

                bleeding.StepsCount--;
                if (bleeding.StepsCount <= 0) {
                    unitParams.Owner.Components.Remove(bleeding);
                }
            }

            _eventsManager.Invoke(GameEventType.UpdateUnitParams, new CustomObject(updateEventData));
        }
    }
}
