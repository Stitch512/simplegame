﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Systems {
    /// <summary>
    /// уменьшить базовую атаку противника на Х на 2 хода.
    /// </summary>
    public class CheckAttackReduceProcessing : BaseEntitySystem {
        private readonly IEventsManager<GameEventType> _eventsManager;

        public CheckAttackReduceProcessing(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) : base(
            entityEngine) {
            _eventsManager = eventsManager;
        }

        public override void Execute(CustomObject data) {

            var updateEventData = new UpdateUnitParamsEventData();
            
            var reduces = Engine.GetComponents<AttackReduce>().ToList();
            foreach (var reduce in reduces) {
                if (reduce.IsActive) {
                    reduce.StepsCount--;
                    if (reduce.StepsCount <= 0) {
                        var unitParams = reduce.Owner.GetComponent<UnitParams>();
                        if (unitParams != null) {
                            unitParams.Attack += reduce.Reduce;
                            unitParams.Owner.Components.Remove(reduce);
                            updateEventData.UnitsParams.Add(new UpdateUnitParams {
                                UnitId = new ElementId(ElementType.Unit, unitParams.Owner.Id),
                                Attack = new ItemValue<int> { Value = unitParams.Attack }
                            });
                        }
                    }
                }
                else {
                    reduce.IsActive = true;
                    var unitParams = reduce.Owner.GetComponent<UnitParams>();
                    if (unitParams != null) {
                        unitParams.Attack -= reduce.Reduce;
                        updateEventData.UnitsParams.Add(new UpdateUnitParams {
                            UnitId = new ElementId(ElementType.Unit, unitParams.Owner.Id), 
                            Attack = new ItemValue<int> { Value = unitParams.Attack }
                        });
                    }
                }
            }

            _eventsManager.Invoke(GameEventType.UpdateUnitParams, new CustomObject(updateEventData));
        }
    }
}