﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Systems
{
    public sealed class ActivateNextUnitPocessing : BaseEntitySystem
    {
        private readonly IEventsManager<GameEventType> _eventsManager;

        public ActivateNextUnitPocessing(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) : base(entityEngine) {
            _eventsManager = eventsManager;
        }

        public override void Execute(CustomObject data) {

            var session = Engine.GetComponents<GameSession>().FirstOrDefault();
            if (session == null || session.SessionState != GameSessionState.ActionProcessing)
                return;

            if (SelectNextUnit(session.CurrentTeam, session)) {
                return;
            }

            int currTeam = session.CurrentTeam;
            do {
                currTeam = ++currTeam % session.TeamsCount;

                if (SelectNextUnit(currTeam, session)) {
                    session.CurrentTeam = currTeam;
                    session.ActivatedUnits.Clear();
                    //todo: change team event
                    break;
                }
            }
            while (currTeam != session.CurrentTeam);

            //todo: level complete
        }

        private bool SelectNextUnit(int team, GameSession session) {
            var nextUnit = GetNextUnit(team, session);
            if (nextUnit == null)
                return false;
            var selectEventData = new UnitSelectEventData {
                SelectElementId = new ElementId(ElementType.Unit, nextUnit.Owner.Id),
                OldSelectElementId = session.ActiveUnit != null ? new ElementId(ElementType.Unit, session.ActiveUnit.Id) : null
            };
            session.ActiveUnit = nextUnit.Owner;
            //session.ActivatedUnits.Add(nextUnit.Owner);
            session.SessionState = GameSessionState.WaitAction;
            //todo: chage selected event
            _eventsManager.Invoke(GameEventType.ActiveUnitChange, new CustomObject(selectEventData));
            return true;
        }

        private UnitTeam GetNextUnit(int team, GameSession session) {
            return Engine.GetComponents<UnitTeam>(c => {
                                                      return c.Owner.GetComponents<UnitParams>().FirstOrDefault()?.HP > 0 && 
                                                             c.Team == team && 
                                                             !session.ActivatedUnits.Contains(c.Owner);
                                                  }).FirstOrDefault();
        }
    }
}
