﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Level;

namespace Assets.Scripts.Game.Systems {
    public class LoadFieldProcessing: BaseEntitySystem {
        private readonly IEventsManager<GameEventType> _eventsManager;

        public LoadFieldProcessing(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) : base(entityEngine) {
            _eventsManager = eventsManager;
        }

        public override void Execute(CustomObject data) {
            var levelInfo = data.Value as LevelInfo;
            if (levelInfo == null)
                return;

            var session = Engine.GetComponent<GameSession>();

            var createElementsData = new List<CreateElementEventData>();
            var updateParamsData = new UpdateUnitParamsEventData();

            foreach (var unitData in levelInfo.Units) {
                var unit = Engine.CreateEntity();
                var coord = Engine.AddComponent<ElementTransform>(unit);
                coord.Position = unitData.Position;
                var team = Engine.AddComponent<UnitTeam>(unit);
                team.Team = unitData.TeamId;
                var param = Engine.AddComponent<UnitParams>(unit);
                param.AbilityType = unitData.AbilityType;
                switch (unitData.EffectType) {
                    case EffectType.AddAttack:
                        var effect = Engine.AddComponent<AddAttackEffect>(unit);
                        effect.AddedAttack = 3;
                        break;
                }
                param.HP = unitData.HP;
                param.Attack = unitData.Attack;
                var unitId = new ElementId(ElementType.Unit, unit.Id);
                updateParamsData.UnitsParams.Add(new UpdateUnitParams {
                    UnitId = unitId,
                    HP = new ItemValue<int> { Value = param.HP },
                    Attack = new ItemValue<int> { Value = param.Attack }
                });
                createElementsData.Add(new CreateElementEventData { Id = unitId, Position = coord.Position });
            }

            session.SessionState = GameSessionState.ActionProcessing;

            _eventsManager.Invoke(GameEventType.CreateElements, new CustomObject(createElementsData));
            _eventsManager.Invoke(GameEventType.UpdateUnitParams, new CustomObject(updateParamsData));
        }

    }
}
