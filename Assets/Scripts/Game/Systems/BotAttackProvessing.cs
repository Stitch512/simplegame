﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Systems {
    public class BotAttackProvessing : BaseEntitySystem {
        public BotAttackProvessing(IEntityEngine entityEngine) : base(entityEngine) { }

        public override void Execute(CustomObject data) {
            var session = Engine.GetComponent<GameSession>();
            if (session == null ||
                session.CurrentTeam == session.MyTeam ||
                session.ActiveUnit == null)
                return;

            var unit = Engine.GetComponents<UnitTeam>(c => c.Team != session.CurrentTeam).FirstOrDefault();
            if (unit == null)
                return;

            session.SelectedUnit = unit.Owner;
        }
    }
}