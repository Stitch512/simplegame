﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Game.Systems.Ability;

namespace Assets.Scripts.Game.Systems {
    public class ApplyAbilityProcessing : BaseEntitySystem {
        private readonly IEventsManager<GameEventType> _eventsManager;

        private readonly Dictionary<AbilityType, IAbility> _abilities = new Dictionary<AbilityType, IAbility>();

        public ApplyAbilityProcessing(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) :
            base(entityEngine) {
            _eventsManager = eventsManager;

            _abilities.Add(AbilityType.Attack, new AttackAbility());
            _abilities.Add(AbilityType.Attack2, new AttackAbility2(Engine));
        }

        public override void Execute(CustomObject data) {
            var session = Engine.GetComponent<GameSession>();
            if (session == null /*|| session.SessionState == GameSessionState.ActionProcessing*/)
                return;

            if (session.ActiveUnit == null || session.SelectedUnit == null)
                return;

            if (session.ActiveUnit.GetComponent<UnitTeam>().Team == session.SelectedUnit.GetComponent<UnitTeam>().Team)
                return;

            var unitParams = session.ActiveUnit.GetComponent<UnitParams>();
            if (unitParams == null)
                return;

            var selUnitParams = session.SelectedUnit.GetComponent<UnitParams>();
            if (selUnitParams == null)
                return;

            IAbility ability;
            if (!_abilities.TryGetValue(unitParams.AbilityType, out ability))
                return;

            ability.Apply(unitParams, selUnitParams);
            session.DamagedUnits.Add(selUnitParams.Owner.Id);

            session.SessionState = GameSessionState.ActionProcessing;
            session.ActivatedUnits.Add(unitParams.Owner);

            _eventsManager.Invoke(GameEventType.AttackUnit,
                new CustomObject(new AttackEventData
                {
                    AttackType = AbilityType.Attack,
                    SourceElementId = new ElementId(ElementType.Unit, session.ActiveUnit.Id),
                    TargetElementId = new ElementId(ElementType.Unit, session.SelectedUnit.Id),
                    HP = selUnitParams.HP
                }));

            session.SelectedUnit = null;

            /*switch (unitParams.AbilityType) {
                case AbilityType.Attack:
                    selUnitParams.HP -= unitParams.Attack;



                    _eventsManager.Invoke(GameEventType.AttackUnit,
                        new CustomObject(new AttackEventData {
                            AttackType = AbilityType.Attack,
                            SourceElementId = new ElementId(ElementType.Unit, session.ActiveUnit.Id),
                            TargetElementId = new ElementId(ElementType.Unit, session.SelectedUnit.Id),
                            HP = selUnitParams.HP
                        }));

                    session.SessionState = GameSessionState.ActionProcessing;
                    session.ActivatedUnits.Add(unitParams.Owner);
                    session.SelectedUnit = null;
                    break;
            }*/
        }
    }
}