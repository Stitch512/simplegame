﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Systems {
    public abstract class BaseEntitySystem : IEntitySystem {
        protected IEntityEngine Engine;

        public BaseEntitySystem(IEntityEngine entityEngine) {
            Engine = entityEngine;
        }

        public virtual void Initialize() {
        }

        public abstract void Execute(CustomObject data);
    }
}