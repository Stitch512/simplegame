﻿using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Systems {
    public class CheckDeathUnitsProcessing : BaseEntitySystem {
        private readonly IEventsManager<GameEventType> _eventsManager;

        public CheckDeathUnitsProcessing(IEntityEngine entityEngine, IEventsManager<GameEventType> eventsManager) : base(
            entityEngine) {
            _eventsManager = eventsManager;
        }

        public override void Execute(CustomObject data) {

            var destroyEventData = new DestroyElementsEventData();
            var deatUnits = Engine.GetComponents<UnitParams>(c => c.HP <= 0).ToList();
            foreach (var unit in deatUnits) {
                Engine.RemoveEntity(unit.Owner.Id);
                destroyEventData.Elements.Add(new ElementEventData { Id = new ElementId(ElementType.Unit, unit.Owner.Id)});
            }

            _eventsManager.Invoke(GameEventType.DeathUnit, new CustomObject(destroyEventData));
        }
    }
}