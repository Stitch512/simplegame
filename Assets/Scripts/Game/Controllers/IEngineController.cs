﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Controllers
{
    public interface IEngineController {
        void AddSystem(GameEventType eventType, IEntitySystem entitySystem);

        void InvokeEvent(GameEventType eventType, CustomObject data);
    }
}
