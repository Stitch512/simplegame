﻿
using System;
using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Controllers;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Game.Controllers
{
    public class GameCommandStorage: IControllerCommandStorage<GameEventType>
    {
        private Dictionary<GameEventType, Action<CustomObject, Action<CustomObject>>> _commandStorage = new Dictionary<GameEventType, Action<CustomObject, Action<CustomObject>>>();

        public Action<CustomObject, Action<CustomObject>> GetAction(GameEventType actionType)
        {
            Action<CustomObject, Action<CustomObject>> action;
            if (!_commandStorage.TryGetValue(actionType, out action))
                return null;
            return action;
        }


    }
}
