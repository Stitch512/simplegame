﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Game.Systems;

namespace Assets.Scripts.Game.Controllers {
    public class MainEngineController : IEngineController, ICustomUpdatable {
        private IEntityEngine _entityEngine;

        private IEventsManager<GameEventType> _eventsManager;

        private readonly Dictionary<GameEventType, IEntitySystem> _entitySystems =
            new Dictionary<GameEventType, IEntitySystem>();

        public MainEngineController() { }

        public void AddSystem(GameEventType eventType, IEntitySystem entitySystem) {
            _entitySystems.Add(eventType, entitySystem);
        }

        public void InvokeEvent(GameEventType eventType, CustomObject data) {
            IEntitySystem system;
            if (!_entitySystems.TryGetValue(eventType, out system))
                return;
            system.Execute(data);
        }

        private void Initialize() {
            var sessionEntity = _entityEngine.CreateEntity();
            var session = _entityEngine.AddComponent<GameSession>(sessionEntity);
            session.CurrentTeam = 0;
            session.MyTeam = 0;

            AddSystem(GameEventType.InitializeField,
                new EntitySystemsGroup(new LoadFieldProcessing(_entityEngine, _eventsManager),
                    new ActivateNextUnitPocessing(_entityEngine, _eventsManager)));

            AddSystem(GameEventType.SelectUnit,
                new EntitySystemsGroup(new SelectUnitProcessing(_entityEngine),
                    new ApplyAbilityProcessing(_entityEngine, _eventsManager),
                    new CheckAttackReduceProcessing(_entityEngine, _eventsManager),
                    new CheckBleedingProcessing(_entityEngine, _eventsManager),
                    new CheckAddAttackEffectProcessind(_entityEngine, _eventsManager),
                    new CheckDeathUnitsProcessing(_entityEngine, _eventsManager)));

            AddSystem(GameEventType.AttackUnitComplete,
                new EntitySystemsGroup(new ActivateNextUnitPocessing(_entityEngine, _eventsManager),
                    new BotAttackProvessing(_entityEngine), new ApplyAbilityProcessing(_entityEngine, _eventsManager),
                    new CheckAttackReduceProcessing(_entityEngine, _eventsManager),
                    new CheckBleedingProcessing(_entityEngine, _eventsManager),
                    new CheckAddAttackEffectProcessind(_entityEngine, _eventsManager),
                    new CheckDeathUnitsProcessing(_entityEngine, _eventsManager)));

            foreach (var systems in _entitySystems.Values) { systems.Initialize(); }
        }

        public void CustomUpdate() { }

        public static Builder With() => new Builder(new MainEngineController());

        public class Builder {
            private readonly MainEngineController _engineController;

            internal Builder(MainEngineController engineController) {
                _engineController = engineController;
            }

            public Builder AddEntityEngine(IEntityEngine entityEngine) {
                _engineController._entityEngine = entityEngine;
                return this;
            }

            public Builder AddEventsManager(IEventsManager<GameEventType> eventsManager) {
                _engineController._eventsManager = eventsManager;
                return this;
            }

            public MainEngineController Build() {
                _engineController.Initialize();
                return _engineController;
            }
        }
    }
}