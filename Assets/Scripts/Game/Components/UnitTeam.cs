﻿using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Components
{
    public class UnitTeam: Component
    {
        public int Team;
    }
}
