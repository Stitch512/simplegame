﻿using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Components
{
    public class AddAttackEffect: Component {
        public int AddedAttack;
    }
}
