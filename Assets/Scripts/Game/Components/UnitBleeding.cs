﻿using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Components
{
    public class UnitBleeding: Component {
        /// <summary> На какое значение уменьшатся жизни </summary>
        public int IncrementValue;

        /// <summary> На сколько ходов уменьшаются жизни </summary>
        public int StepsCount;
    }
}
