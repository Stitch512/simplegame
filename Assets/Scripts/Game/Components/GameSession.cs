﻿using System.Collections.Generic;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Game.Common;

namespace Assets.Scripts.Game.Components {

    public enum GameSessionState {
        Undefined,
        /// <summary> Ироговое поле не загружено </summary>
        NotInitialize,
        /// <summary> Ожидание действия </summary>
        WaitAction,
        /// <summary> Происходит атака на юнита </summary>
        //AttackProcessing,
        /// <summary> Происходит действие </summary>
        ActionProcessing,
    }

    public class GameSession : Component {

        /// <summary> Текущее состояние игры </summary>
        public GameSessionState SessionState = GameSessionState.Undefined;

        /// <summary> Кол-во команд </summary>
        public int TeamsCount = 2;

        /// <summary> Команда, которая сейчас делает ход </summary>
        public int CurrentTeam = -1;

        /// <summary> Команда, за которую играет игрок </summary>
        public int MyTeam;

        /// <summary> Юниты, текущей команды, которые уже сделали выстрел </summary>
        public List<IEntity> ActivatedUnits = new List<IEntity>();

        /// <summary> Текущий юнит, который применяет способность </summary>
        public IEntity ActiveUnit;

        /// <summary> Выбранный юнит, на которого применяется способность </summary>
        public IEntity SelectedUnit;

        /// <summary> Юниты, которые получили повреждение на последнем шаге </summary>
        public List<int> DamagedUnits = new List<int>();


    }
}