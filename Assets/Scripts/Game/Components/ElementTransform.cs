﻿using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Components
{
    public class ElementTransform: Component {
        public UnityEngine.Vector3 Position;

        public UnityEngine.Vector3 Rotation;
    }
}
