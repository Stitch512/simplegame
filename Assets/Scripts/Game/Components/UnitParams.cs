﻿using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Components {
    public enum AbilityType {
        None,
        /// <summary> Нанести урон равный базовой атаке </summary>
        Attack,
        /// <summary> Нанести урон равный половине базовой атаке и уменьшить базовую атаку противника на Х на 2 хода </summary>
        Attack2,
        /// <summary> Снять все эффекты с юнита и наложить кровотечение, которое наносит урон равный половине базовой атаке каждый ход </summary>
        Bleeding
    }

    public enum EffectType {
        None,
        /// <summary> При получении урона увеличивает свою атаку на Х </summary>
        AddAttack,
        /// <summary> При смерти союзника применяет свою способность на случайную цель </summary>
        AbilityOnDeath
    }

    public class UnitParams : Component {
        public AbilityType AbilityType;

        public EffectType EffectType;

        public int HP;

        public int Attack;
    }
}