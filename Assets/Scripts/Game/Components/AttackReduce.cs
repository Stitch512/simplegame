﻿using Assets.Scripts.Common.ECS;

namespace Assets.Scripts.Game.Components
{
    public class AttackReduce: Component {
        /// <summary> На какое значение уменьшиться атака </summary>
        public int Reduce;

        /// <summary> На сколько ходов уменьшиться атака </summary>
        public int StepsCount;

        /// <summary> Было ли применено действие </summary>
        public bool IsActive;
    }
}
