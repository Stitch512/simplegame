﻿namespace Assets.Scripts.Game.Common
{
    public enum ElementType
    {
        Undefined,
        Unit,
        Rocket
    }
}
