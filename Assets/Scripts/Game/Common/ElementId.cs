﻿namespace Assets.Scripts.Game.Common {
    public sealed class ElementId {
        public ElementType ElementType { get; }

        public int Id { get; }

        private readonly string _cacheToString;

        public ElementId(ElementType elementType, int id) {
            ElementType = elementType;
            Id = id;
            _cacheToString = $"{ElementType}_{Id}";
        }

        public override bool Equals(object obj) {
            var otherId = obj as ElementId;
            if (otherId == null)
                return false;
            return otherId.ElementType == ElementType && otherId.Id == Id;
        }

        public static bool operator ==(ElementId obj1, ElementId obj2) {
            if (ReferenceEquals(obj1, null))
                return ReferenceEquals(obj2, null);
            return obj1.Equals(obj2);
        }

        public static bool operator !=(ElementId obj1, ElementId obj2) {
            return !(obj1 == obj2);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = ElementType.GetHashCode();
                hashCode = (hashCode * 397) ^ Id.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString() {
            return _cacheToString;
        }
    }
}