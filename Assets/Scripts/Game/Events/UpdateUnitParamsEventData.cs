﻿using System.Collections.Generic;
using Assets.Scripts.Game.Common;

namespace Assets.Scripts.Game.Events
{
    public class ItemValue<T> {
        public T Value;

        public T Delta;
    }

    public class UpdateUnitParams {
        public ElementId UnitId;

        public ItemValue<int> HP;

        public ItemValue<int> Attack;
    }

    public class UpdateUnitParamsEventData {

        public List<UpdateUnitParams> UnitsParams = new List<UpdateUnitParams>();

    }
}
