﻿using Assets.Scripts.Game.Common;

namespace Assets.Scripts.Game.Events
{
    public class UnitSelectEventData {
        public ElementId SelectElementId;

        public ElementId OldSelectElementId;
    }
}
