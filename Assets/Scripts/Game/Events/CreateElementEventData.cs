﻿using Assets.Scripts.Game.Common;
using UnityEngine;

namespace Assets.Scripts.Game.Events {
    public class CreateElementEventData {
        public ElementId Id;

        public Vector3 Position;
    }
}