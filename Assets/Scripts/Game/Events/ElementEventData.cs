﻿using Assets.Scripts.Game.Common;

namespace Assets.Scripts.Game.Events {
    public class ElementEventData {
        public ElementId Id;
    }
}