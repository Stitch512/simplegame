﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Events
{
    public class DestroyElementsEventData
    {
        public List<ElementEventData> Elements = new List<ElementEventData>();
    }
}
