﻿using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Components;

namespace Assets.Scripts.Game.Events
{
    public class AttackEventData {
        public AbilityType AttackType;

        public ElementId SourceElementId;

        public ElementId TargetElementId;

        public int HP;
    }
}
