﻿
namespace Assets.Scripts.Game.Events
{
    public enum GameEventType
    {
        Undefined,

        EventMouseDown,
        EventMouseDoubleClick,
        EventMouseUp,
        EventMouseMove,

        /// <summary> Загрузка игры </summary>
        InitializeField,
        /// <summary> Создание элемента </summary>
        CreateElements,
        /// <summary> Смена текущего юнита </summary>
        ActiveUnitChange,
        /// <summary> Выбор юнита для атаки </summary>
        SelectUnit,
        /// <summary> Атака юнита </summary>
        AttackUnit,
        /// <summary> Атака юнита завершена </summary>
        AttackUnitComplete,
        /// <summary> Обновить параметры юнита </summary>
        UpdateUnitParams,
        /// <summary> Юнит помер </summary>
        DeathUnit


    }
}
