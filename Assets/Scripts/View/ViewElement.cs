﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Events;
using UnityEngine;

namespace Assets.Scripts.View
{
    public class ViewElement: MonoBehaviour, IViewElement
    {
        public virtual void InitializeEvents(IEventsManager<GameEventType> eventsManager)
        {
            
        }

        public GameObject Owner => this.gameObject;

        public bool IsDisposed { get; set; }

        public virtual void OnCreate(CustomObject data)
        {

        }

        public virtual void OnDelete()
        {

        }
    }
}
