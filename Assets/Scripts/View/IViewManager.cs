﻿using System.Collections.Generic;
using Assets.Scripts.Game.Common;

namespace Assets.Scripts.View
{
    public interface IViewManager
    {
        IViewElement CreateVisual(ElementId elementId);

        void RemoveVisual(IViewElement element);

        void RemoveVisual(ElementId elementId);

        IViewElement GetVisual(ElementId elementId);

        IEnumerable<KeyValuePair<ElementId, IViewElement>> GetElements();

        void RemoveAll();
    }
}
