﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.Binders;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.View {
    public sealed class InputManager : MonoBehaviour, IBindable<GameEventType> {
        private IEventsManager<GameEventType> _eventsManager;

        private const float _deltaRegistration = 10;
        private bool _isMouseDown;
        private bool _isOneClick;
        private float _doubleClickTime;
        private float delay = 0.6f;
        private Vector2 _previousMousePosition = Vector2.zero;

        public void Bind(IEventsManager<GameEventType> eventsManager) {
            _eventsManager = eventsManager;
        }

        public Vector3 ButtonUpPosition { get; private set; }

        public Vector3 ButtonDownPosition { get; private set; }

        public Vector3 MovePosition { get; private set; }

        private void Update() {
            if (IsOverGUI()) {
                _isMouseDown = false;
                _isOneClick = false;
                _previousMousePosition = Vector2.zero;
                return;
            }

            if (Input.GetKeyDown(KeyCode.Mouse0)) {
                _isMouseDown = true;
                MovePosition = ButtonDownPosition = Input.mousePosition;

                if (!_isOneClick) {
                    _isOneClick = true;
                    _doubleClickTime = Time.time;
                    _eventsManager.Invoke(GameEventType.EventMouseDown, new CustomObject(Input.mousePosition));
                }
                else {
                    _isOneClick = false;
                    _eventsManager.Invoke(GameEventType.EventMouseDoubleClick, new CustomObject(Input.mousePosition));
                    MovePosition = ButtonDownPosition = ButtonUpPosition = Input.mousePosition;
                    return;
                }
            }

            if (_isOneClick) {
                if ((Time.time - _doubleClickTime) > delay) {
                    _isOneClick = false;
                }
            }

            if (!_isMouseDown) return;

            if (Input.GetKeyUp(KeyCode.Mouse0)) {
                _isMouseDown = false;

                MovePosition = ButtonUpPosition = Input.mousePosition;
                _eventsManager.Invoke(GameEventType.EventMouseUp, new CustomObject(Input.mousePosition));
                return;
            }

            if ((Mathf.Abs(_previousMousePosition.x - Input.mousePosition.x) > _deltaRegistration
                 || Mathf.Abs(_previousMousePosition.y - Input.mousePosition.y) > _deltaRegistration)
                && (Mathf.Abs(ButtonDownPosition.x - Input.mousePosition.x) > _deltaRegistration
                    || Mathf.Abs(ButtonDownPosition.y - Input.mousePosition.y) > _deltaRegistration)) {
                _isOneClick = false;
                MovePosition = _previousMousePosition = Input.mousePosition;
                _eventsManager.Invoke(GameEventType.EventMouseMove, new CustomObject(_deltaRegistration));
            }
        }

        private bool IsOverGUI() {
            if (EventSystem.current == null)
                return false;
            if (EventSystem.current.IsPointerOverGameObject())
                return true;

            if (Input.touchCount > 0 && Input.GetTouch(0).phase != TouchPhase.Ended) {
                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                    return true;
            }

            return false;
        }
    }
}