﻿using Assets.Scripts.Common.Commands;
using DG.Tweening;

namespace Assets.Scripts.View.Commands {
    public class ShowViewCommand: Command {
        private IViewElement _view;

        private readonly float _delay;

        private Tween _tween;

        public ShowViewCommand(IViewElement view, float delay = 0) {
            _view = view;
            _delay = delay;
        }

        protected override void OnExecute() {
            _tween = DOVirtual.DelayedCall(_delay, Finish, false);
        }

        protected override void OnComplete()
        {
            if(!_view.IsDisposed)
                _view.Owner.SetActive(true);
            Clear();
        }

        protected override void OnTerminate()
        {
            _tween?.Kill(false);
            if(!_view.IsDisposed)
                _view.Owner.SetActive(true);
            Clear();
        }

        private void Clear()
        {
            _view = null;
            _tween = null;
        }
    }
}