﻿using Assets.Scripts.Common.Commands;
using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.View.Commands {
    public class MoveViewCommand : Command {
        private IViewElement _view;
        private readonly Vector3 _position;
        private readonly float _time;
        private Tween _tween;

        public MoveViewCommand(IViewElement view, Vector3 pos, float time) {
            _view = view;
            _position = pos;
            _time = time;
        }

        protected override void OnExecute() {
            if (_view.IsDisposed) {
                Finish();
                return;
            }

            //_view.DisposedEvent += OnViewDisposed;

            _tween = _view.Owner.transform.DOLocalMove(_position, _time);
            _tween.OnComplete(Finish);
        }

        protected override void OnComplete() {
            Clear();
        }

        protected override void OnTerminate() {
            _tween?.Kill(false);
            Clear();
        }

        private void OnViewDisposed(IViewElement view) {
            _tween?.Kill(false);
            Finish();
        }

        private void Clear() {
            //if (_view != null && !_view.IsDisposed)
            //    _view.DisposedEvent -= OnViewDisposed;
            _tween = null;
            _view = null;
        }
    }
}