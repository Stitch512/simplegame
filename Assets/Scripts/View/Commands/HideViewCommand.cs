﻿using Assets.Scripts.Common.Commands;

namespace Assets.Scripts.View.Commands {
    public class HideViewCommand : Command {
        private IViewElement _view;

        public HideViewCommand(IViewElement view) {
            _view = view;
        }

        protected override void OnExecute() {
            Finish();
        }

        protected override void OnComplete() {
            if (!_view.IsDisposed)
                _view.Owner.SetActive(false);
            Clear();
        }

        protected override void OnTerminate() {
            if (!_view.IsDisposed)
                _view.Owner.SetActive(false);
            Clear();
        }

        private void Clear() {
            _view = null;
        }
    }
}