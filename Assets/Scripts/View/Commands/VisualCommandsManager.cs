﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Common;

namespace Assets.Scripts.View.Commands {
    public class VisualCommandsManager : IVisualCommandsManager {
        private readonly ICommandsManager _commandsManager;

        private readonly Dictionary<ElementId, List<ICommand>> _commands = new Dictionary<ElementId, List<ICommand>>();

        public VisualCommandsManager(ICommandsManager commandsManager) {
            _commandsManager = commandsManager;
        }

        public void ExecuteCommand(ElementId elementId, ICommand command) {
            List<ICommand> cmdStack;
            if (!_commands.TryGetValue(elementId, out cmdStack)) {
                _commands[elementId] = new List<ICommand> { command };
                _commandsManager.ExecuteCommand(command, cmd => CompleteAction(elementId, command));
            }
            else {
                _commands[elementId].Add(command);
            }
        }

        private void CompleteAction(ElementId elementId, ICommand command) {
            List<ICommand> cmdStack;
            if (!_commands.TryGetValue(elementId, out cmdStack))
                return;
            cmdStack.Remove(command);
            if (cmdStack.Count > 0) {
                var nextCmd = cmdStack[0];
                _commandsManager.ExecuteCommand(nextCmd, cmd => CompleteAction(elementId, nextCmd));
            }
            else {
                _commands.Remove(elementId);
            }
        }
    }
}