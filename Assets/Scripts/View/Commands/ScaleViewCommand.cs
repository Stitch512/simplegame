﻿using Assets.Scripts.Common.Commands;
using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.View.Commands {
    public class ScaleViewCommand : Command {
        private IViewElement _view;
        private readonly Vector3 _scale;
        private readonly float _duration;
        private readonly float _delay;
        private readonly Ease _easeType;
        private readonly bool _yoyo;

        private Tween _tween;

        public ScaleViewCommand(IViewElement view, Vector3 scale, float duration, float delay = 0,
                                Ease easeType = Ease.Linear, bool yoyo = false) {
            _view = view;
            _scale = scale;
            _duration = duration;
            _delay = delay;
            _easeType = easeType;
            _yoyo = yoyo;
        }

        protected override void OnExecute() {
            if (_view.IsDisposed) {
                Finish();
                return;
            }

            //_view.DisposedEvent += OnViewDisposed;

            _tween = _view.Owner.transform.DOScale(_scale, _duration).SetEase(_easeType).SetDelay(_delay);
            _tween.OnComplete(Finish);

            if (_yoyo)
                _tween.SetLoops(2, LoopType.Yoyo);
        }

        protected override void OnComplete() {
            Clear();
        }

        protected override void OnTerminate() {
            _tween?.Kill(false);
            Clear();
        }

        private void OnViewDisposed(IViewElement view) {
            _tween?.Kill(false);
            Finish();
        }

        private void Clear() {
            //if (_view != null && !_view.IsDisposed)
            //    _view.IVisualElement -= OnViewDisposed;

            _tween = null;
            _view = null;
        }
    }
}