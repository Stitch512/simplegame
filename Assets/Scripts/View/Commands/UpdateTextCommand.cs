﻿using System.Linq;
using Assets.Scripts.Common.Commands;
using UnityEngine;

namespace Assets.Scripts.View.Commands {
    public class UpdateTextCommand : Command {
        private IViewElement _view;

        private readonly string _text;

        private readonly string _elementName;

        public UpdateTextCommand(IViewElement view, string text, string elementName = "") {
            _view = view;
            _text = text;
            _elementName = elementName;
        }

        protected override void OnExecute() {
            var textMeshes = _view.Owner.GetComponentsInChildren<TextMesh>();
            if (textMeshes.Length > 0) {
                if (!string.IsNullOrEmpty(_elementName)) {
                    textMeshes = textMeshes.Where(c => c.gameObject.name == _elementName).ToArray();
                }
                foreach (var textMesh in textMeshes) {
                    textMesh.text = _text;
                }
            }

            Finish();
        }

        protected override void OnComplete() {
            Clear();
        }

        protected override void OnTerminate() {
        }

        private void Clear() {
            _view = null;
        }
    }
}