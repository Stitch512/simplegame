﻿using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Common;

namespace Assets.Scripts.View.Commands {
    public class DestroyViewCommand : Command {
        private IViewElement _view;
        private IViewManager _viewManager;

        public DestroyViewCommand(IViewElement view, IViewManager viewManager) {
            _view = view;
            _viewManager = viewManager;
        }

        protected override void OnExecute() {
            _viewManager.RemoveVisual(_view);
            Finish();
        }

        protected override void OnComplete() {
            Clear();
        }

        protected override void OnTerminate() {
            //_viewManager.RemoveVisual(_view);
            Clear();
        }

        private void Clear() {
            _viewManager = null;
            _view = null;
        }
    }
}