﻿
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Common;

namespace Assets.Scripts.View.Commands
{
    public interface IVisualCommandsManager {
        void ExecuteCommand(ElementId elementId, ICommand command);
    }
}
