﻿using Assets.Scripts.Common.Commands;
using UnityEngine;

namespace Assets.Scripts.View.Commands {
    public class SetMaterialColorCommand : Command {
        private IViewElement _view;

        private readonly Color _color;

        public SetMaterialColorCommand(IViewElement view, Color color) {
            _view = view;
            _color = color;
        }

        protected override void OnExecute() {
            if (!_view.IsDisposed) {
                var renderer = _view.Owner.GetComponentInChildren<MeshRenderer>();
                if (renderer != null && renderer.material != null) {
                    renderer.sharedMaterial.color = _color;
                }
            }

            Finish();
        }

        protected override void OnComplete() {
            Clear();
        }

        protected override void OnTerminate() {
            if (!_view.IsDisposed)
                _view.Owner.SetActive(false);
            Clear();
        }

        private void Clear() {
            _view = null;
        }
    }
}