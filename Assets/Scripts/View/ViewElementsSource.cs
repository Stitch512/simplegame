﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Game.Common;
using UnityEngine;

namespace Assets.Scripts.View
{
    [CreateAssetMenu(fileName = "ElementsProto", menuName = "Game/ElementsProto", order = 1)]
    public class ViewElementsSource: ScriptableObject, IViewElementsSource
    {
        [Serializable]
        private class ElementProto
        {
            public ElementType ElementType;
            public ViewElement Proto;
        }

        [SerializeField]
        private List<ElementProto> _elements;


        public IViewElement GetProto(ElementType elementType) {
            var proto = _elements.FirstOrDefault(e => e.ElementType == elementType);
            if (proto == null || proto.Proto == null)
                return null;
            return proto.Proto;
        }
    }
}
