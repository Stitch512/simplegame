﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.View.Actions {
    public class CreateElementsAction : IViewAction {
        private readonly IViewManager _viewManager;

        public CreateElementsAction(IViewManager viewManager) {
            _viewManager = viewManager;
        }

        public void Apply(CustomObject data, Action<CustomObject> postAction) {
            var createData = data.Value as List<CreateElementEventData>;
            if (createData == null)
                return;

            foreach (var elementData in createData) {
                var elementView = _viewManager.CreateVisual(elementData.Id);
                elementView.Owner.transform.localPosition = elementData.Position;
            }

            postAction.Invoke(CustomObject.Empty);
        }
    }
}