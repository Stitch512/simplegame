﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Events;
using Assets.Scripts.View.Commands;
using UnityEngine;

namespace Assets.Scripts.View.Actions
{
    public class AttackUnitAction : IViewAction
    {
        private readonly IViewManager _viewManager;
        private readonly IVisualCommandsManager _visualCommandsManager;

        public AttackUnitAction(IViewManager viewManager, IVisualCommandsManager visualCommandsManager)
        {
            _viewManager = viewManager;
            _visualCommandsManager = visualCommandsManager;
        }

        public void Apply(CustomObject data, Action<CustomObject> postAction)
        {
            var evementData = data.Value as AttackEventData;
            if (evementData == null)
                return;

            var sourceUnit = _viewManager.GetVisual(evementData.SourceElementId);
            if (sourceUnit == null)
                return;

            var targetUnit = _viewManager.GetVisual(evementData.TargetElementId);
            if (targetUnit == null)
                return;

            var rocketUnit = _viewManager.CreateVisual(new ElementId(ElementType.Rocket, 0));
            rocketUnit.Owner.transform.localPosition = sourceUnit.Owner.transform.localPosition;

            var rocketCmd = new SequenceCommand(
                    new MoveViewCommand(rocketUnit, targetUnit.Owner.transform.localPosition, 0.5f),
                    new DestroyViewCommand(rocketUnit, _viewManager),
                    new UpdateTextCommand(targetUnit, $"HP: {evementData.HP}", "LabelHP")
                );

            _visualCommandsManager.ExecuteCommand(evementData.TargetElementId, rocketCmd);

            new CommandsObserver(rocketCmd).StartObserve(() => {
                                                             postAction.Invoke(CustomObject.Empty);
                                                         });
        }
    }
}
