﻿using System;
using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.View.Actions
{
    public class MouseClickAction : IViewAction
    {
        private readonly IViewManager _viewManager;

        public MouseClickAction(IViewManager viewManager)
        {
            _viewManager = viewManager;
        }

        public void Apply(CustomObject data, Action<CustomObject> postAction)
        {
            Vector3 mousePosition = (Vector3)data.Value;

            var ray = Camera.main.ScreenPointToRay(mousePosition);

            foreach (var elementData in _viewManager.GetElements()) {
                var visual = elementData.Value;
                var colliders = visual.Owner.GetComponentsInChildren<Collider>();
                foreach (var collider in colliders) {
                    RaycastHit hit;
                    if (collider.Raycast(ray, out hit, int.MaxValue)) {
                        postAction.Invoke(new CustomObject(elementData.Key));
                        return;
                    }
                }
            }

            postAction.Invoke(CustomObject.Empty);
        }
    }
}
