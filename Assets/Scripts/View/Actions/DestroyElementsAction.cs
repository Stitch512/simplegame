﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Events;
using Assets.Scripts.View.Commands;

namespace Assets.Scripts.View.Actions {
    public class DestroyElementsAction : IViewAction {
        private readonly IViewManager _viewManager;
        private readonly IVisualCommandsManager _visualCommandsManager;

        public DestroyElementsAction(IViewManager viewManager, IVisualCommandsManager visualCommandsManager) {
            _viewManager = viewManager;
            _visualCommandsManager = visualCommandsManager;
        }

        public void Apply(CustomObject data, Action<CustomObject> postAction) {
            var updateData = data.Value as DestroyElementsEventData;
            if (updateData == null)
                return;

            var destroyCommands = new ParallelCommand();
            foreach (var unitData in updateData.Elements) {
                var unit = _viewManager.GetVisual(unitData.Id);
                if (unit == null)
                    continue;
                var destroyCmd = new DestroyViewCommand(unit, _viewManager);
                destroyCommands.Add(destroyCmd);
                _visualCommandsManager.ExecuteCommand(unitData.Id, destroyCmd);
            }

            var observer = new CommandsObserver(destroyCommands);
            observer.StartObserve(() => { postAction.Invoke(CustomObject.Empty); });
        }
    }
}