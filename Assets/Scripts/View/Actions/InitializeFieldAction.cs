﻿using System;
using Assets.Scripts.Common;

namespace Assets.Scripts.View.Actions
{
    public class InitializeFieldAction : IViewAction
    {
        public void Apply(CustomObject data, Action<CustomObject> postAction)
        {
            postAction.Invoke(data);
        }
    }
}
