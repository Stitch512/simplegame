﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Events;
using Assets.Scripts.View.Commands;
using UnityEngine;

namespace Assets.Scripts.View.Actions {
    public class ActiveUnitChangeAction : IViewAction {
        private readonly IViewManager _viewManager;
        private readonly ICommandsManager _commandsManager;

        public ActiveUnitChangeAction(IViewManager viewManager, ICommandsManager commandsManager) {
            _viewManager = viewManager;
            _commandsManager = commandsManager;
        }

        public void Apply(CustomObject data, Action<CustomObject> postAction) {
            var evementData = data.Value as UnitSelectEventData;
            if (evementData == null)
                return;

            if (evementData.OldSelectElementId != null) {
                var oldVisual = _viewManager.GetVisual(evementData.OldSelectElementId);
                if (oldVisual != null) {
                    _commandsManager.ExecuteCommand(new ScaleViewCommand(oldVisual, Vector3.one, 0.5f));
                }
            }

            var visual = _viewManager.GetVisual(evementData.SelectElementId);
            if (visual != null) {
                _commandsManager.ExecuteCommand(new ScaleViewCommand(visual, Vector3.one * 1.5f, 0.5f));
            }

            postAction.Invoke(CustomObject.Empty);
        }
    }
}