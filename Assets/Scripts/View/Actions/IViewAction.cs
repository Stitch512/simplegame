﻿
using System;
using Assets.Scripts.Common;

namespace Assets.Scripts.View.Actions
{
    public interface IViewAction
    {
        void Apply(CustomObject data, Action<CustomObject> postAction);
    }
}
