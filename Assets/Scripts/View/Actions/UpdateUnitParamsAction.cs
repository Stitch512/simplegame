﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Events;
using Assets.Scripts.View.Commands;

namespace Assets.Scripts.View.Actions {
    public class UpdateUnitParamsAction : IViewAction {
        private readonly IViewManager _viewManager;
        private readonly IVisualCommandsManager _visualCommandsManager;

        public UpdateUnitParamsAction(IViewManager viewManager, IVisualCommandsManager visualCommandsManager) {
            _viewManager = viewManager;
            _visualCommandsManager = visualCommandsManager;
        }

        public void Apply(CustomObject data, Action<CustomObject> postAction) {
            var updateData = data.Value as UpdateUnitParamsEventData;
            if (updateData == null)
                return;

            foreach (var unitData in updateData.UnitsParams) {
                var changeCmd = new ParallelCommand();
                var unit = _viewManager.GetVisual(unitData.UnitId);
                if (unit == null)
                    continue;

                if (unitData.HP != null) {
                    changeCmd.Add(new UpdateTextCommand(unit, $"HP: {unitData.HP.Value}", "LabelHP"));
                }

                if (unitData.Attack != null) {
                    changeCmd.Add(new UpdateTextCommand(unit, $"Attack: {unitData.Attack.Value}", "AttackHP"));
                }

                _visualCommandsManager.ExecuteCommand(unitData.UnitId, changeCmd);
            }

            postAction.Invoke(CustomObject.Empty);
        }
    }
}