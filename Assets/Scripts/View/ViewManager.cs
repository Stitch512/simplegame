﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Game.Common;
using UnityEngine;

namespace Assets.Scripts.View {
    public class ViewManager : IViewManager {
        private readonly IViewElementsFactory _elementsFactory;

        private readonly Dictionary<ElementId, IViewElement> _elements = new Dictionary<ElementId, IViewElement>();

        public ViewManager(IViewElementsFactory elementsFactory) {
            _elementsFactory = elementsFactory;
        }

        public IViewElement CreateVisual(ElementId elementId) {
            var view = _elementsFactory.Create(elementId);
            if (view == null)
                return null;
            _elements[elementId] = view;
            return view;
        }

        public void RemoveVisual(IViewElement element) {
            var item = _elements.FirstOrDefault(e => e.Value == element);
            if (item.Key == null)
                return;
            GameObject.Destroy(element.Owner);
            _elements.Remove(item.Key);
        }

        public void RemoveVisual(ElementId elementId) {
            IViewElement element;
            if (!_elements.TryGetValue(elementId, out element))
                return;
            GameObject.Destroy(element.Owner);
            _elements.Remove(elementId);
        }

        public IViewElement GetVisual(ElementId elementId) {
            if (elementId == null)
                return null;
            IViewElement element;
            if (!_elements.TryGetValue(elementId, out element))
                return null;
            return element;
        }

        public IEnumerable<KeyValuePair<ElementId, IViewElement>> GetElements() {
            return _elements;
        }

        public void RemoveAll() {
            throw new System.NotImplementedException();
        }
    }
}