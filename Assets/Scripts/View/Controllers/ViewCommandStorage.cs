﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Common.Controllers;
using Assets.Scripts.Game.Events;
using Assets.Scripts.View.Actions;
using Assets.Scripts.View.Commands;

namespace Assets.Scripts.View.Controllers
{
    public class ViewCommandStorage: IControllerCommandStorage<GameEventType>
    {
        private IViewManager _viewManager;

        private ICommandsManager _commandsManager;

        private IVisualCommandsManager _visualCommandsManager;

        private Dictionary<GameEventType, IViewAction> _actions;

        public static Builder With() => new Builder(new ViewCommandStorage());

        public Action<CustomObject, Action<CustomObject>> GetAction(GameEventType actionType)
        {
            IViewAction action;
            if (!_actions.TryGetValue(actionType, out action))
                return null;
            return action.Apply;
        }

        private void Initialize()
        {
            _actions = new Dictionary<GameEventType, IViewAction>
            {
                { GameEventType.InitializeField, new InitializeFieldAction() },
                { GameEventType.CreateElements, new CreateElementsAction(_viewManager) },
                { GameEventType.ActiveUnitChange, new ActiveUnitChangeAction(_viewManager, _commandsManager) },
                { GameEventType.EventMouseDown, new MouseClickAction(_viewManager) },
                { GameEventType.AttackUnit, new AttackUnitAction(_viewManager, _visualCommandsManager) },
                { GameEventType.UpdateUnitParams, new UpdateUnitParamsAction(_viewManager, _visualCommandsManager) },
                { GameEventType.DeathUnit, new DestroyElementsAction(_viewManager, _visualCommandsManager) },
            };
        }

        public class Builder
        {
            private readonly ViewCommandStorage _commandStorage;

            internal Builder(ViewCommandStorage commandStorage)
            {
                _commandStorage = commandStorage;
            }

            public Builder AddVisualsManager(IViewManager viewManager)
            {
                _commandStorage._viewManager = viewManager;
                return this;
            }

            public Builder AddCommandsManager(ICommandsManager commandsManager)
            {
                _commandStorage._commandsManager = commandsManager;
                return this;
            }

            public Builder AddVisualCommandsManager(IVisualCommandsManager visualCommandsManager)
            {
                _commandStorage._visualCommandsManager = visualCommandsManager;
                return this;
            }

            public IControllerCommandStorage<GameEventType> Build()
            {
                _commandStorage.Initialize();
                return _commandStorage;
            }
        }
    }
}
