﻿
using Assets.Scripts.Common.Controllers;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.View.Controllers
{
    public class ViewController: BaseController<GameEventType>
    {
        public ViewController(IControllerCommandStorage<GameEventType> commandStorage) : base(commandStorage)
        {

        }
    }
}
