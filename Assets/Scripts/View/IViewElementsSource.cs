﻿using Assets.Scripts.Game.Common;

namespace Assets.Scripts.View
{
    public interface IViewElementsSource {
        IViewElement GetProto(ElementType elementType);
    }
}
