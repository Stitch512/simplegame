﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Events;
using UnityEngine;

namespace Assets.Scripts.View
{
    public interface IViewElement
    {
        void InitializeEvents(IEventsManager<GameEventType> eventsManager);

        GameObject Owner { get; }

        bool IsDisposed { get; set; } // todo: private setter

        void OnCreate(CustomObject data);

        void OnDelete();
    }
}
