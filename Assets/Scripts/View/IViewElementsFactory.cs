﻿using Assets.Scripts.Game.Common;

namespace Assets.Scripts.View
{
    public interface IViewElementsFactory
    {
        IViewElement Create(ElementId elementId);
    }
}
