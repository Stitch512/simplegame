﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Binders;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.View.Configuration {
    public class RelationConfig {
        private readonly IBinder<GameEventType> _binder;

        private GameEventType _eventType;

        private Func<CustomObject, CustomObject> _getModelParam;

        private Action<CustomObject> _postAction;

        public RelationConfig(IBinder<GameEventType> binder) {
            _binder = binder;
        }

        private CustomObject GetModelParam(CustomObject obj) {
            return _getModelParam.Invoke(obj);
        }

        private void PostAction(CustomObject postCustomObject) {
            _postAction.Invoke(postCustomObject);
        }

        public void Bind() {
            _binder.Bind(_eventType, GetModelParam, PostAction);
        }

        public static Builder With() => new Builder();

        public class Builder {
            private IBinder<GameEventType> _binder;
            private RelationConfig _relationConfig;
            private GameEventType _eventType;
            private Func<CustomObject, CustomObject> _getParams = obj => obj /*CustomObject.Empty*/;
            private Action<CustomObject> _postAction = posObj => { };

            internal Builder() { }

            public Builder AddBinder(IBinder<GameEventType> binder) {
                _binder = binder;
                return this;
            }

            public Builder AddEventType(GameEventType eventType) {
                _eventType = eventType;
                return this;
            }

            public Builder AddGetModelParam(Func<CustomObject, CustomObject> getParams) {
                _getParams = getParams;
                return this;
            }

            public Builder AddPostAction(Action<CustomObject> postAction) {
                _postAction = postAction;
                return this;
            }

            public RelationConfig Build() {
                _relationConfig = new RelationConfig(_binder) {
                    _getModelParam = _getParams,
                    _eventType = _eventType,
                    _postAction = _postAction
                };
                _relationConfig.Bind();
                return _relationConfig;
            }
        }
    }
}