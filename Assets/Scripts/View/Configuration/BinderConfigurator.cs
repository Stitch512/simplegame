﻿using Assets.Scripts.Common;
using Assets.Scripts.Common.Binders;
using Assets.Scripts.Game.Controllers;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.View.Configuration {
    public class BinderConfigurator {
        private IBinder<GameEventType> _binder;

        private IEngineController _engineController;

        public BinderConfigurator() { }

        public void Initialize() {
            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.InitializeField)
                          .AddPostAction(postObj => {
                                             _engineController.InvokeEvent(GameEventType.InitializeField, postObj);
                                         }).Build();

            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.CreateElements)
                          .Build();

            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.ActiveUnitChange)
                          .Build();

            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.EventMouseDown)
                          .AddPostAction(postObj => {
                                             if (postObj.Status != CustomObjectStatus.Empty)
                                                _engineController.InvokeEvent(GameEventType.SelectUnit, postObj);
                                         }).Build();

            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.AttackUnit)
                          .AddPostAction(postObj => {
                                             _engineController.InvokeEvent(GameEventType.AttackUnitComplete, postObj);
                                         })
                          .Build();

            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.UpdateUnitParams)
                          .Build();

            RelationConfig.With().AddBinder(_binder).AddEventType(GameEventType.DeathUnit)
                          .AddPostAction(postObj => {
                                         })
                          .Build();

        }

        public static Builder With() => new Builder(new BinderConfigurator());

        public class Builder {
            private readonly BinderConfigurator _configurator;

            internal Builder(BinderConfigurator configurator) {
                _configurator = configurator;
            }

            public Builder AddBinder(IBinder<GameEventType> binder) {
                _configurator._binder = binder;
                return this;
            }

            public Builder AddEngineController(IEngineController engineController) {
                _configurator._engineController = engineController;
                return this;
            }

            public BinderConfigurator Build() {
                _configurator.Initialize();
                return _configurator;
            }
        }
    }
}