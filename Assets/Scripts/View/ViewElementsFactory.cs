﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.Binders;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Common;
using Assets.Scripts.Game.Events;
using UnityEngine;

namespace Assets.Scripts.View {
    public class ViewElementsFactory: IViewElementsFactory {
        private readonly IViewElementsSource _elementsSource;
        private readonly IEventsManager<GameEventType> _eventsManager;

        public ViewElementsFactory(IViewElementsSource elementsSource, IEventsManager<GameEventType> eventsManager) {
            _elementsSource = elementsSource;
            _eventsManager = eventsManager;
        }

        public IViewElement Create(ElementId elementId) {
            var proto = _elementsSource.GetProto(elementId.ElementType);
            if (proto == null)
                return null;

            var instance = GameObject.Instantiate<GameObject>(proto.Owner);
            var visual = instance.GetComponent<IViewElement>();
            if (visual == null) {
                GameObject.DestroyImmediate(instance);
                return null;
            }

            visual.Owner.name = elementId.ToString();

            var bindables = visual.Owner.GetComponentsInChildren<IBindable<GameEventType>>();
            foreach (var bindable in bindables) {
                bindable.Bind(_eventsManager);
            }

            return visual;
        }
    }
}