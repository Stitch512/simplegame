﻿using System.Collections.Generic;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Binders;
using Assets.Scripts.Common.Commands;
using Assets.Scripts.Common.Controllers;
using Assets.Scripts.Common.ECS;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Components;
using Assets.Scripts.Game.Controllers;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Level;
using Assets.Scripts.View;
using Assets.Scripts.View.Commands;
using Assets.Scripts.View.Configuration;
using Assets.Scripts.View.Controllers;
using UnityEngine;

namespace Assets.Scripts
{
    public class Bootstraper: MonoBehaviour
    {
        [SerializeField]
        private ViewElementsSource _elementsSource;

        [SerializeField]
        private InputManager _inputManager;

        private IEventsManager<GameEventType> _eventsManager;

        private void Start() {

            _eventsManager = new EventsManager();

            _inputManager.Bind(_eventsManager);

            IEntityEngine entityEngine = new EntityEngine();

            IEngineController engineController = MainEngineController.With()
                                                                     .AddEntityEngine(entityEngine)
                                                                     .AddEventsManager(_eventsManager).Build();

            IViewElementsFactory elementsFactory = new ViewElementsFactory(_elementsSource, _eventsManager);

            IViewManager viewManager = new ViewManager(elementsFactory);

            ICommandsManager commandsManager = new CommandsManager();
            IVisualCommandsManager visualCommandsManager = new VisualCommandsManager(commandsManager);

            IControllerCommandStorage<GameEventType> commandStorage = ViewCommandStorage.With()
                                                                                          .AddVisualsManager(viewManager)
                                                                                          .AddCommandsManager(commandsManager)
                                                                                          .AddVisualCommandsManager(visualCommandsManager)
                                                                                          .Build();

            IController<GameEventType, CustomObject> controller = new ViewController(commandStorage);
            IBinder<GameEventType> binder = new BaseBinder<GameEventType>(_eventsManager, controller);

            var configurator = BinderConfigurator.With()
                                                 .AddBinder(binder)
                                                 .AddEngineController(engineController).Build();

            CreateScene();
        }

        private void CreateScene() {
            var levelInfo = new LevelInfo {
                Units = new List<UnitInfo> {
                    new UnitInfo {
                        TeamId = 0, AbilityType = AbilityType.Attack, Attack = 30, HP = 100, Position = new Vector3(0, 0, 0), EffectType = EffectType.AddAttack
                    },
                    new UnitInfo {
                        TeamId = 0, AbilityType = AbilityType.Attack2, Attack = 30, HP = 90, Position = new Vector3(5, 0, 0)
                    },
                    new UnitInfo {
                        TeamId = 1, AbilityType = AbilityType.Attack, Attack = 30, HP = 80, Position = new Vector3(0, 0, 5)
                    },
                    new UnitInfo {
                        TeamId = 1, AbilityType = AbilityType.Attack2, Attack = 30, HP = 25, Position = new Vector3(5, 0, 5)
                    },
                }
            };

            _eventsManager.Invoke(GameEventType.InitializeField, new CustomObject(levelInfo));
        }
    }
}
