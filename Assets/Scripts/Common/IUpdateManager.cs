﻿
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public enum UpdatablesName
    {
        Undefined,
        InputManager,
        MainEngineController
    }
    /// <summary>
    ///  Common Update interface with ICustomUpdatable children
    /// </summary>
    public interface IUpdateManager : ICoroutine
    {
        void Start();

        void Stop();

        void AddUpdatableComponent(UpdatablesName updaters, ICustomUpdatable customUpdatable);
        void PauseUpdatableComponen(UpdatablesName updaters);

        void ResumUpdatableComponen(UpdatablesName updaters);
    }

    /// <summary>
    /// Updatable object interface
    /// </summary>
    public interface ICustomUpdatable
    {
        void CustomUpdate();
    }

    /// <summary>
    /// Coroutine executor interface
    /// </summary>
    public interface ICoroutine
    {
        Coroutine StartCoroutine(IEnumerator routine);
    }
}
