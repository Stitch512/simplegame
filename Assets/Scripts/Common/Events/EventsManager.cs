﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Common.Events
{
    public class EventsManager: IEventsManager<GameEventType>
    {
        private readonly Dictionary<GameEventType, List<Action<CustomObject>>> _listeners = new Dictionary<GameEventType, List<Action<CustomObject>>>(); 

        public void Invoke(GameEventType gameEventType, CustomObject data)
        {
            List<Action<CustomObject>> listeners;
            if (!_listeners.TryGetValue(gameEventType, out listeners))
                return;

            foreach (var listener in listeners)
            {
                listener?.Invoke(data);
            }
        }

        public void AddListener(GameEventType gameEventType, Action<CustomObject> callback)
        {
            List<Action<CustomObject>> listeners;
            if (!_listeners.TryGetValue(gameEventType, out listeners))
            {
                listeners = new List<Action<CustomObject>>();
                _listeners[gameEventType] = listeners;
            }
            if (listeners.Contains(callback))
                return;
            listeners.Add(callback);
        }

        public void RemoveListener(GameEventType gameEventType, Action<CustomObject> callback)
        {
            List<Action<CustomObject>> listeners;
            if (!_listeners.TryGetValue(gameEventType, out listeners))
                return;
            listeners.Remove(callback);
        }
    }
}
