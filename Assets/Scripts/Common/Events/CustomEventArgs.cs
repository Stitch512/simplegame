﻿using System;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Common.Events
{
    public class CustomEventArgs: EventArgs
    {
        public GameEventType GameEventType { get; private set; }
        public object Sender { get; private set; }
        public CustomObject Data { get; private set; }

        public CustomEventArgs(GameEventType gameEventType, object sender, CustomObject data)
        {
            GameEventType = gameEventType;
            Sender = sender;
            Data = data;
        }

        public override string ToString()
        {
            return $"[EventArgs: GameEventType={GameEventType}, Sender={Sender}, Data={Data}";
        }

    }
}
