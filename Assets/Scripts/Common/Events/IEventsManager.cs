﻿using System;

namespace Assets.Scripts.Common.Events
{
    //public delegate void CustomEvent(CustomEventArgs args);

    public interface IEventsManager<in T> where T: struct
    {
        void Invoke(T eventType, CustomObject data);

        void AddListener(T unitEnum, Action<CustomObject> callback);

        void RemoveListener(T unitEnum, Action<CustomObject> callback);
    }
}
