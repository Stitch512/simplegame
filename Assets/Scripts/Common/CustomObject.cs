﻿using System;

namespace Assets.Scripts.Common
{
    public enum CustomObjectStatus
    {
        Undefined,
        Empty,
        Valid
    }

    public class CustomObject
    {
        private static readonly CustomObject _empty = new CustomObject();

        public CustomObject(Object value)
        {
            Status = value != null ? CustomObjectStatus.Valid : CustomObjectStatus.Empty;
            Value = value;
        }

        private CustomObject()
        {
            Status = CustomObjectStatus.Empty;
            Value = null;
        }

        public override string ToString()
        {
            return Value == null ? "Null object" : Value.ToString();
        }

        public Object Value { get; private set; }
        public String ErrorMessage { get; set; }

        public CustomObjectStatus Status { get; private set; }

        public static CustomObject Empty
        {
            get { return _empty; }
        }
    }
}
