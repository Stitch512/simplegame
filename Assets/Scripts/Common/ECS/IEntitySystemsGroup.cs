﻿using System.Collections.Generic;

namespace Assets.Scripts.Common.ECS
{
    public interface IEntitySystemsGroup: IEntitySystem
    {
        List<IEntitySystem> Systems { get; }

        void AddSystem(IEntitySystem system);

        void RemoveSystem<T>() where T : IEntitySystem;
    }
}
