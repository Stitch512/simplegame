﻿namespace Assets.Scripts.Common.ECS
{
    public interface IComponent {
        IEntity Owner { get; set; }
    }
}
