﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Common.ECS {
    public interface IEntity {
        int Id { get; }

        List<IComponent> Components { get; }

        T GetComponent<T>() where T : class, IComponent;

        T GetComponent<T>(Func<T, bool> condition) where T : class, IComponent;

        IEnumerable<T> GetComponents<T>() where T : class, IComponent;

        IEnumerable<T> GetComponents<T>(Func<T, bool> cindition) where T : class, IComponent;
    }
}