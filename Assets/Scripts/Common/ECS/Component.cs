﻿namespace Assets.Scripts.Common.ECS
{
    public class Component: IComponent
    {
        public IEntity Owner { get; set; }
    }
}
