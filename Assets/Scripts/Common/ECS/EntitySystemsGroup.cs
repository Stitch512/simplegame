﻿using System.Collections.Generic;

namespace Assets.Scripts.Common.ECS
{
    public class EntitySystemsGroup: IEntitySystemsGroup
    {
        private readonly List<IEntitySystem> _systems = new List<IEntitySystem>();

        public EntitySystemsGroup()
        {
        }

        public EntitySystemsGroup(params IEntitySystem[] systems)
        {
            _systems.AddRange(systems);
        }

        public List<IEntitySystem> Systems => _systems;

        public void Initialize() {
            _systems.ForEach(s => s.Initialize());
        }

        public void Execute(CustomObject data)
        {
            _systems.ForEach(s => s.Execute(data));
        }

        public void AddSystem(IEntitySystem system)
        {
            _systems.Add(system);
        }

        public void RemoveSystem<T>() where T : IEntitySystem
        {
            for (int i = _systems.Count - 1; i >= 0; i--)
            {
                if (_systems[i] is T)
                    _systems.RemoveAt(i);
            }
        }
    }
}
