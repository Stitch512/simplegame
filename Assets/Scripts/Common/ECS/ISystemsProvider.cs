﻿namespace Assets.Scripts.Common.ECS
{
    public interface ISystemsProvider
    {
        void AddSystem(IEntitySystem system);

        void RermoveSystem<T>() where T : IEntitySystem;

        void RermoveAllSystems();

        void Run();
    }
}
