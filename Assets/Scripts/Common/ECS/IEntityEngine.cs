﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Common.ECS {
    public interface IEntityEngine {
        IEntity CreateEntity();

        bool RemoveEntity(int id);

        IEntity GetEntity(int entityId);

        IEntity CreateEntityWith<T>() where T : class, IComponent, new();

        T AddComponent<T>(int entityId) where T : class, IComponent, new();

        T AddComponent<T>(IEntity entity) where T : class, IComponent, new();

        void RemoveComponent<T>(IEntity entity) where T : class, IComponent, new();

        T GetComponent<T>() where T : class, IComponent;

        T GetComponent<T>(Func<T, bool> condition) where T : class, IComponent;

        IEnumerable<T> GetComponents<T>() where T : class, IComponent;

        IEnumerable<T> GetComponents<T>(Func<T, bool> cindition) where T : class, IComponent;

    }
}
