﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Common.ECS {
    public class EntityEngine : IEntityEngine {
        private readonly Dictionary<int, IEntity> _entities = new Dictionary<int, IEntity>();

        private static int _nextId;

        public IEntity CreateEntity() {
            IEntity entity = new Entity(getNextId());
            _entities[entity.Id] = entity;
            return entity;
        }

        public IEntity GetEntity(int entityId) {
            IEntity entity;
            if (!_entities.TryGetValue(entityId, out entity)) { return null; }

            return entity;
        }

        public IEntity CreateEntityWith<T>() where T : class, IComponent, new() {
            var entity = CreateEntity();
            AddComponent<T>(entity);
            return entity;
        }

        public T AddComponent<T>(int entityId) where T : class, IComponent, new() {
            var entity = GetEntity(entityId);
            if (entity == null)
                return null;
            return AddComponent<T>(entity);
        }

        public T AddComponent<T>(IEntity entity) where T : class, IComponent, new() {
            if (entity == null) { return null; }
            var component = new T();
            component.Owner = entity;
            entity.Components.Add(component);
            return component;
        }

        public void RemoveComponent<T>(IEntity entity) where T : class, IComponent, new() {
            if (entity == null) { return; }
        }

        public IEnumerable<T> GetComponents<T>() where T : class, IComponent {
            return _entities.Values.SelectMany(e => e.GetComponents<T>());
        }

        public IEnumerable<T> GetComponents<T>(Func<T, bool> condition) where T : class, IComponent {
            return _entities.Values.SelectMany(e => e.GetComponents<T>(condition));
        }

        public T GetComponent<T>() where T : class, IComponent {
            return GetComponents<T>().FirstOrDefault();
        }

        public T GetComponent<T>(Func<T, bool> condition) where T : class, IComponent {
            return GetComponents<T>().FirstOrDefault(condition);
        }


        public bool RemoveEntity(int id) {
            IEntity entity;
            if (!_entities.TryGetValue(id, out entity))
                return false;
            _entities.Remove(id);
            return true;
        }

        private int getNextId() {
            return ++_nextId;
        }
    }
}