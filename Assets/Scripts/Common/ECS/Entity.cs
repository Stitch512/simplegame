﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Common.ECS {
    public class Entity : IEntity {
        public Entity(int id) {
            Id = id;
            Components = new List<IComponent>();
        }

        public int Id { get; }

        public List<IComponent> Components { get; }

        public IEnumerable<T> GetComponents<T>() where T : class, IComponent {
            foreach (var component in Components) {
                if (component is T)
                    yield return component as T;
            }
        }

        public IEnumerable<T> GetComponents<T>(Func<T, bool> condition) where T : class, IComponent {
            foreach (var component in Components) {
                var comp = component as T;
                if (comp != null && condition(comp))
                    yield return comp;
            }
        }

        public T GetComponent<T>() where T : class, IComponent {
            return GetComponents<T>().FirstOrDefault();
        }

        public T GetComponent<T>(Func<T, bool> condition) where T : class, IComponent {
            return GetComponents<T>(condition).FirstOrDefault();
        }

    }
}