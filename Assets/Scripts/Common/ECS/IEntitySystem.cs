﻿namespace Assets.Scripts.Common.ECS
{
    public interface IEntitySystem {
        void Initialize();

        void Execute(CustomObject data);
    }
}
