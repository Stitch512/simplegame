﻿
using System;

namespace Assets.Scripts.Common.Controllers
{
    public interface IController<T, U> where T : struct where U : class
    {
        void ApplyAction(T logicUnit, U someObject, Action<U> postActionUI);
    }
}
