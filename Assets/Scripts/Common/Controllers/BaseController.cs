﻿
using System;

namespace Assets.Scripts.Common.Controllers
{
    public class BaseController<T> : IController<T, CustomObject> where T : struct
    {
        private readonly IControllerCommandStorage<T> _commandStorage;

        public BaseController(IControllerCommandStorage<T> commandStorage)
        {
            _commandStorage = commandStorage;
        }

        public void ApplyAction(T logicUnit, CustomObject data, Action<CustomObject> postAction)
        {
            var action = _commandStorage.GetAction(logicUnit);
            if (action == null)
                return;
            action.Invoke(data, postAction);
        }
    }
}
