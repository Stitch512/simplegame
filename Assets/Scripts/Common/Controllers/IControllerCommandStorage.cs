﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Common.Controllers
{
    public interface IControllerCommandStorage<T> where T : struct
    {
        //void AddAction(T actionType, Action<CustomObject, Action<CustomObject>> action);

        Action<CustomObject, Action<CustomObject>> GetAction(T actionType);
    }

}
