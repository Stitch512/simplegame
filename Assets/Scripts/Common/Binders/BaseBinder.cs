﻿
using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Controllers;
using Assets.Scripts.Common.Events;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Common.Binders
{
    //public delegate void BindAction(GameEventType logicAtom, IEventSource multipleEventSource, Func<CustomObject, CustomObject> getParameters, Action<CustomObject> postAction);


    public class BaseBinder<T>: IBinder<T> where T : struct
    {
        private readonly IEventsManager<T> _eventsManager;

        private readonly IController<T, CustomObject> _controller;

        public BaseBinder(IEventsManager<T> eventsManager, IController<T, CustomObject> controller)
        {
            _eventsManager = eventsManager;
            _controller = controller;
        }

        public void Bind(IEventSource<T> eventSource, T logicUnit, Func<CustomObject, CustomObject> getParameters, Action<CustomObject> postAction)
        {
            if (eventSource == null)
                throw new Exception("Can not provide cast operation");

            eventSource.AddListener(logicUnit, (obj) => {
                                                   var uiInput = getParameters.Invoke(obj);
                                                   _controller.ApplyAction(logicUnit, uiInput, postAction);
                                               });
        }

        public void Bind(T logicUnit, Func<CustomObject, CustomObject> getParameters, Action<CustomObject> postAction)
        {
            _eventsManager.AddListener(logicUnit, args => {
                                                     var uiInput = getParameters.Invoke(args);
                                                     _controller.ApplyAction(logicUnit, uiInput, postAction);
                                                 });
        }

        public void UnBind()
        {

        }
    }
}
