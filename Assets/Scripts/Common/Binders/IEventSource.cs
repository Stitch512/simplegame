﻿
using System;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Common.Binders
{
    public interface IEventSource<in T> where T : struct
    {
        void AddListener(T logicAtom, Action<CustomObject> action);

        void RemoveListener(T logicAtom);
    }
}
