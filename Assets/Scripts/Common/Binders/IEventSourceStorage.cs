﻿namespace Assets.Scripts.Common.Binders
{
    public interface IEventSourceStorage<in T> where T : struct
    {
        IEventSource<T> GetEventSource(T eventType);

        void Notify(T eventType, CustomObject obj);
    }
}