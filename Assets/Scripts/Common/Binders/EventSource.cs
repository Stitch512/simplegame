﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Events;

namespace Assets.Scripts.Common.Binders
{
    /*public class EventManager: IEventSource<GameEventType>
    {
        private readonly Dictionary<GameEventType, Action<CustomObject>> _activities = new Dictionary<GameEventType, Action<CustomObject>>();


        public EventManager()
        {
            _activities = new Dictionary<GameEventType, Action<CustomObject>>();
        }

        public void AddListener(GameEventType logicAtom, Action<CustomObject> action)
        {
            if (_activities.ContainsKey(logicAtom))
            {
                Debug.LogWarning(ErrorMessages.ErrorTryAddMultipleLogicForAtom + logicAtom);
                //return;
            }
            _activities[logicAtom] = action;
        }


        public void RemoveListener(GameEventType logicAtom)
        {
            if (_activities.ContainsKey(logicAtom) == false)
            {
                return;
            }
            _activities.Remove(logicAtom);
        }

        public void RaiseEvent(GameEventType logicAtom, CustomObject obj)
        {
            if (_activities.ContainsKey(logicAtom) == false)
            {
                return;
            }
            _activities[logicAtom].Invoke(obj);
        }

    }*/
}
