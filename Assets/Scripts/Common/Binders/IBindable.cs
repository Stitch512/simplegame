﻿using Assets.Scripts.Common.Events;

namespace Assets.Scripts.Common.Binders
{
    public interface IBindable<T> where T : struct
    {
        void Bind(IEventsManager<T> eventsManager);
    }
}
