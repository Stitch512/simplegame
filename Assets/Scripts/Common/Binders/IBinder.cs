﻿
using System;

namespace Assets.Scripts.Common.Binders
{
    public interface IBinder<T> where T : struct
    {
        void Bind(IEventSource<T> eventSource, T logicUnit, Func<CustomObject, CustomObject> getParametersUI, Action<CustomObject> postActionUI);

        void Bind(T logicUnit, Func<CustomObject, CustomObject> getParametersUI, Action<CustomObject> postActionUI);

    }
}
